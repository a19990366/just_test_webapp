const container = document.getElementById("mynetwork");
// provide data in the DOT language
var DOTstring = `dinetwork {A -> B; B -> D; A -> C; C -> D }`;
var parsedData = vis.network.convertDot(DOTstring);

var data = {
  nodes: parsedData.nodes,
  edges: parsedData.edges
};

var options = parsedData.options;

// you can extend the options like a normal JSON variable:
options.nodes = {
  color: "#ffffff"
};

// create a network
var network = new vis.Network(container, data, options);
